//@ sourceMappingURL=johodb.map
(function() {
  'use strict';
  var READ_WRITE_FLAG, excludesToSQL, lookupsToSQL, matchLookups, matchSteps, stErr, stSuc, stepsToSQL, __deleteCursorPromise, _ref, _ref1, _ref10, _ref11, _ref12, _ref2, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.JohoDB = (function() {
    function JohoDB(name, version) {
      this.name = name;
      this.version = version;
      this.rawSchema = {};
    }

    JohoDB.prototype.addSchema = function(schemaName, schema) {
      return this.rawSchema[schemaName] = schema;
    };

    JohoDB.prototype.initDB = function(storageType, opts) {
      var key, model, _ref, _ref1;

      this.__buildJSSchema();
      if (storageType === "indexedDb") {
        this.store = new JohoDB.IndexedDbStorage(this.name, this.version);
        _ref = this.models;
        for (key in _ref) {
          model = _ref[key];
          model.store = this.store;
        }
      } else if (storageType === "webSql") {
        this.store = new JohoDB.WebSqlStorage(this.name, this.version);
        _ref1 = this.models;
        for (key in _ref1) {
          model = _ref1[key];
          model.store = this.store;
        }
      }
      return this.store.initDB({
        models: this.models,
        relationTables: this.relationTables
      }, opts);
    };

    JohoDB.prototype.__buildJSSchema = function() {
      var modelName, modelSchema, _ref;

      this.models = {};
      _ref = this.rawSchema;
      for (modelName in _ref) {
        if (!__hasProp.call(_ref, modelName)) continue;
        modelSchema = _ref[modelName];
        this.models[modelName] = new JohoDB.Model(modelName, modelSchema);
      }
      return this.__setupRelations();
    };

    JohoDB.prototype.__setupRelations = function() {
      var model, modelName, _ref;

      this.relationTables = {};
      this.relations = [];
      _ref = this.models;
      for (modelName in _ref) {
        if (!__hasProp.call(_ref, modelName)) continue;
        model = _ref[modelName];
        this.relations = this.relations.concat(model.getRelations());
      }
      this.__linkPrimaryKeys();
      return this.__formRelations();
    };

    JohoDB.prototype.__linkPrimaryKeys = function() {
      var r, _i, _len, _ref, _results;

      _ref = this.relations;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        r = _ref[_i];
        r.from.primaryKey = this.models[r.from.modelName].primaryKey;
        _results.push(r.to.primaryKey = this.models[r.to.modelName].primaryKey);
      }
      return _results;
    };

    JohoDB.prototype.__formRelations = function() {
      var from, r, to, _i, _len, _ref, _ref1, _results;

      _ref = this.relations;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        r = _ref[_i];
        if (r.requiresRelationTable) {
          this.relationTables[r.getTableName()] = r.buildRelationTable();
        }
        _ref1 = r.buildRelationManagers(), from = _ref1[0], to = _ref1[1];
        this.models[from.ownModel].relations[from.name] = from;
        this.models[from.ownModel].relations[from.name].model = this.models[to.ownModel];
        this.models[to.ownModel].relations[to.name] = to;
        _results.push(this.models[to.ownModel].relations[to.name].model = this.models[from.ownModel]);
      }
      return _results;
    };

    return JohoDB;

  })();

  window.JohoDB.Field = (function() {
    function Field(name, traits, skipCheck) {
      var key, value;

      this.name = name;
      if (!skipCheck) {
        if (traits.type.match(/(int|integer)/)) {
          traits.type = "Int";
          return new JohoDB.IntField(this.name, traits, true);
        }
        if (traits.type.match(/(str|string|text)/)) {
          traits.type = "String";
          return new JohoDB.TextField(this.name, traits, true);
        }
        if (traits.type.match(/(datetime)/)) {
          traits.type = "DateTime";
          return new JohoDB.DateTimeField(this.name, traits, true);
        }
        if (traits.type.match(/(fk|foreignKey|foreignkey)/)) {
          traits.type = "ForeignKey";
          return new JohoDB.ForeignKeyField(this.name, traits, true);
        }
        if (traits.type.match(/(m2m|manytomany|manyToMany)/)) {
          traits.type = "ManyToMany";
          return new JohoDB.ManyToManyField(this.name, traits, true);
        }
      }
      for (key in traits) {
        if (!__hasProp.call(traits, key)) continue;
        value = traits[key];
        this[key] = value;
      }
    }

    Field.prototype.defaultValidators = [];

    Field.prototype.validate = function(value) {
      var validator, _i, _j, _len, _len1, _ref, _ref1;

      if (this.required) {
        if (value === void 0) {
          console.error("Validation Error for " + this.name + ", required.");
        }
      }
      if (this.primaryKey) {
        if (!this.autoIncrement && !this.autoHash) {
          if (value === void 0) {
            console.error("Validation Error for " + this.name + ", required primary key / no auto.");
          }
        }
      }
      _ref = this.defaultValidators;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        validator = _ref[_i];
        if (!validator(value)) {
          console.error("Validation Error for " + this.name);
        }
      }
      if (this.validators) {
        _ref1 = this.validators;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          validator = _ref1[_j];
          if (!validator(value)) {
            console.error("Validation Error for " + this.name);
          }
        }
      }
      return true;
    };

    return Field;

  })();

  window.JohoDB.IntField = (function(_super) {
    __extends(IntField, _super);

    function IntField() {
      _ref = IntField.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    IntField.prototype.defaultValidators = [
      function(value) {
        var number;

        number = parseInt(value);
        if (number === NaN) {
          return false;
        }
        return true;
      }
    ];

    return IntField;

  })(JohoDB.Field);

  window.JohoDB.TextField = (function(_super) {
    __extends(TextField, _super);

    function TextField() {
      _ref1 = TextField.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    return TextField;

  })(JohoDB.Field);

  window.JohoDB.DateTimeField = (function(_super) {
    __extends(DateTimeField, _super);

    function DateTimeField() {
      _ref2 = DateTimeField.__super__.constructor.apply(this, arguments);
      return _ref2;
    }

    return DateTimeField;

  })(JohoDB.Field);

  window.JohoDB.RelationField = (function(_super) {
    __extends(RelationField, _super);

    function RelationField() {
      _ref3 = RelationField.__super__.constructor.apply(this, arguments);
      return _ref3;
    }

    return RelationField;

  })(JohoDB.Field);

  window.JohoDB.ForeignKeyField = (function(_super) {
    __extends(ForeignKeyField, _super);

    function ForeignKeyField() {
      _ref4 = ForeignKeyField.__super__.constructor.apply(this, arguments);
      return _ref4;
    }

    ForeignKeyField.prototype.validate = function() {
      return true;
    };

    return ForeignKeyField;

  })(JohoDB.RelationField);

  window.JohoDB.ManyToManyField = (function(_super) {
    __extends(ManyToManyField, _super);

    function ManyToManyField() {
      _ref5 = ManyToManyField.__super__.constructor.apply(this, arguments);
      return _ref5;
    }

    ManyToManyField.prototype.validate = function() {
      return true;
    };

    return ManyToManyField;

  })(JohoDB.RelationField);

  'use strict';

  window.JohoDB.Model = (function() {
    function Model(name, rawSchema) {
      var fieldName, fieldTraits, _ref6;

      this.name = name;
      this.rawSchema = rawSchema;
      this.fields = {};
      this.relations = {};
      _ref6 = this.rawSchema;
      for (fieldName in _ref6) {
        if (!__hasProp.call(_ref6, fieldName)) continue;
        fieldTraits = _ref6[fieldName];
        this.fields[fieldName] = new JohoDB.Field(fieldName, fieldTraits);
      }
      this.primaryKey = this.getPrimaryKey().name;
    }

    Model.prototype.getRelations = function() {
      var field, fieldName, relations, _ref6;

      relations = [];
      _ref6 = this.fields;
      for (fieldName in _ref6) {
        if (!__hasProp.call(_ref6, fieldName)) continue;
        field = _ref6[fieldName];
        if (field.type.match(/(ForeignKey|ManyToMany)/)) {
          relations.push(new JohoDB.Relation(this.name, field));
        }
      }
      return relations;
    };

    Model.prototype.getPrimaryKey = function() {
      var field, fieldName, _ref6;

      _ref6 = this.fields;
      for (fieldName in _ref6) {
        field = _ref6[fieldName];
        if (field.primaryKey) {
          return field;
        }
      }
      throw "No Primary Key found for " + this.name;
    };

    Model.prototype.validate = function(obj) {
      var field, key, relation, _ref6, _ref7, _results;

      _ref6 = this.fields;
      for (key in _ref6) {
        if (!__hasProp.call(_ref6, key)) continue;
        field = _ref6[key];
        field.validate(obj[key]);
      }
      _ref7 = this.relations;
      _results = [];
      for (key in _ref7) {
        if (!__hasProp.call(_ref7, key)) continue;
        relation = _ref7[key];
        _results.push(relation.validate(obj[key]));
      }
      return _results;
    };

    Model.prototype.__reduceObj = function(obj) {
      var key, relation, saveObj, __, _ref6, _ref7;

      saveObj = {};
      _ref6 = this.fields;
      for (key in _ref6) {
        if (!__hasProp.call(_ref6, key)) continue;
        __ = _ref6[key];
        saveObj[key] = obj[key];
      }
      _ref7 = this.relations;
      for (key in _ref7) {
        if (!__hasProp.call(_ref7, key)) continue;
        relation = _ref7[key];
        if (relation.type === "ForeignKey") {
          saveObj[key] = relation.reduce(obj[key]);
        }
        if (relation.type === "ManyToMany") {
          delete saveObj[key];
        }
        if (relation.type === "OneToMany") {
          delete saveObj[key];
        }
      }
      return saveObj;
    };

    Model.prototype.__collectRelatedObjectUpdates = function(obj) {
      var key, newObjs, r, updates, _ref6, _ref7;

      updates = {};
      _ref6 = this.relations;
      for (key in _ref6) {
        if (!__hasProp.call(_ref6, key)) continue;
        r = _ref6[key];
        updates[r.relatedModel] = {
          model: r.model,
          objs: []
        };
      }
      _ref7 = this.relations;
      for (key in _ref7) {
        if (!__hasProp.call(_ref7, key)) continue;
        r = _ref7[key];
        newObjs = r.collectRelatedObjects(obj[key], obj[this.primaryKey]);
        updates[r.relatedModel].objs = updates[r.relatedModel].objs.concat(newObjs);
      }
      return updates;
    };

    Model.prototype.__collectForeignKeyUpdates = function(obj) {
      var key, r, updates, _ref6, _ref7;

      updates = {};
      _ref6 = this.relations;
      for (key in _ref6) {
        if (!__hasProp.call(_ref6, key)) continue;
        r = _ref6[key];
        updates[r.name] = {
          model: r.model,
          name: r.relatedName,
          ids: []
        };
      }
      _ref7 = this.relations;
      for (key in _ref7) {
        if (!__hasProp.call(_ref7, key)) continue;
        r = _ref7[key];
        if (r.type === "OneToMany") {
          updates[r.name].ids = updates[r.name].ids.concat(r.collectKeys(obj[key]));
        }
      }
      return updates;
    };

    Model.prototype.__collectManyToManyUpdates = function(obj) {
      var key, r, updates, _ref6, _ref7;

      updates = {};
      _ref6 = this.relations;
      for (key in _ref6) {
        if (!__hasProp.call(_ref6, key)) continue;
        r = _ref6[key];
        if (r.type === "ManyToMany") {
          updates[r.relationTableName] = {
            name: r.relationTableName,
            updates: [],
            ownField: r.ownModel + '_' + r.ownKey
          };
        }
      }
      _ref7 = this.relations;
      for (key in _ref7) {
        if (!__hasProp.call(_ref7, key)) continue;
        r = _ref7[key];
        if (r.type === "ManyToMany") {
          updates[r.relationTableName].updates = updates[r.relationTableName].updates.concat(r.collectKeys(obj[key], obj[this.primaryKey]));
        }
      }
      return updates;
    };

    Model.prototype.generatePrimaryKey = function(obj) {
      var string;

      string = JSON.stringify(obj);
      return CryptoJS.SHA1(string + new Date().getTime()).toString();
    };

    Model.prototype.__updateForeignKeyRelation = function(objID, fieldName, reverseID) {
      var deferred, p;

      deferred = Q.defer();
      p = this.store.updateForeignKey(this, objID, fieldName, reverseID);
      p.then(function(result) {
        return deferred.resolve(result);
      });
      return deferred.promise;
    };

    Model.prototype.__updateRelations = function(extraUpdates, keyUpdates, m2mUpdates, reverseID) {
      var fieldName, g, modelName, rel, relID, stack, tableName, updateSet, _i, _j, _len, _len1, _ref6, _ref7;

      stack = [];
      if (extraUpdates) {
        for (modelName in extraUpdates) {
          updateSet = extraUpdates[modelName];
          _ref6 = updateSet.objs;
          for (_i = 0, _len = _ref6.length; _i < _len; _i++) {
            rel = _ref6[_i];
            stack.push(updateSet.model.save(rel));
          }
        }
      }
      if (keyUpdates) {
        for (fieldName in keyUpdates) {
          updateSet = keyUpdates[fieldName];
          _ref7 = updateSet.ids;
          for (_j = 0, _len1 = _ref7.length; _j < _len1; _j++) {
            relID = _ref7[_j];
            stack.push(updateSet.model.__updateForeignKeyRelation(relID, updateSet.name, reverseID));
          }
        }
      }
      if (m2mUpdates) {
        for (tableName in m2mUpdates) {
          updateSet = m2mUpdates[tableName];
          stack.push(this.store.clearManyToManyRelations(tableName, reverseID, updateSet.ownField));
          stack.push(this.store.createManyToManyRelations(tableName, updateSet.updates));
        }
      }
      return g = Q.all(stack);
    };

    Model.prototype.__saveRelatedObjects = function(extraUpdates) {
      var stack;

      stack = [];
      return g;
    };

    Model.prototype["delete"] = function(obj, opts) {
      var deferred, query,
        _this = this;

      deferred = Q.defer();
      if (obj[this.primaryKey]) {
        query = {};
        query[this.primaryKey] = obj[this.primaryKey];
        this.query().get(query).evaluate().then(function(trueObj) {
          var g, key, r, relObj, stack, _i, _j, _len, _len1, _ref6, _ref7, _ref8;

          stack = [];
          _ref6 = _this.relations;
          for (key in _ref6) {
            if (!__hasProp.call(_ref6, key)) continue;
            r = _ref6[key];
            if (r.type === "OneToMany") {
              if (r.onDelete === 'cascade' && trueObj[key] !== void 0) {
                _ref7 = trueObj[key];
                for (_i = 0, _len = _ref7.length; _i < _len; _i++) {
                  relObj = _ref7[_i];
                  stack.push(r.model["delete"](relObj));
                }
              } else if (r.onDelete === 'setNull' && trueObj[key] !== void 0) {
                _ref8 = trueObj[key];
                for (_j = 0, _len1 = _ref8.length; _j < _len1; _j++) {
                  relObj = _ref8[_j];
                  relObj[r.relatedName] = void 0;
                  stack.push(r.model.save(relObj));
                }
              }
            }
            if (r.type === "ManyToMany") {
              stack.push(_this.store.clearManyToManyRelations(r.relationTableName, trueObj[_this.primaryKey], r.ownModel + '_' + r.ownKey));
            }
          }
          stack.push(_this.store["delete"](_this, trueObj[_this.primaryKey], {}));
          g = Q.all(stack);
          return g.then(function() {
            return deferred.resolve(true);
          });
        });
      } else {
        console.error("Object was not provided with a primary key", obj);
        deferred.reject("Error");
      }
      return deferred.promise;
    };

    Model.prototype.save = function(obj, opts) {
      var deferred, extraUpdates, keyUpdates, m2mUpdates, p, saveObj,
        _this = this;

      deferred = Q.defer();
      extraUpdates = null;
      keyUpdates = null;
      if (this.validate(obj)) {
        if (obj[this.primaryKey] === void 0) {
          obj[this.primaryKey] = this.generatePrimaryKey(obj);
        }
        saveObj = this.__reduceObj(obj);
        if (opts != null ? opts.updateRelatedObjs : void 0) {
          extraUpdates = this.__collectRelatedObjectUpdates(obj);
          m2mUpdates = this.__collectManyToManyUpdates(obj);
        }
        if (!(opts != null ? opts.updateRelatedObjs : void 0) && (opts != null ? opts.updateRelationships : void 0)) {
          keyUpdates = this.__collectForeignKeyUpdates(obj);
          m2mUpdates = this.__collectManyToManyUpdates(obj);
        }
        p = this.store.save(this, saveObj, opts);
        p = p.then(function(result) {
          var g;

          if (extraUpdates || keyUpdates || m2mUpdates) {
            g = _this.__updateRelations(extraUpdates, keyUpdates, m2mUpdates, obj[_this.primaryKey]);
            return g.then(function(relatedObjs) {
              console.log("Saved " + _this.name + " with Relations", saveObj, relatedObjs);
              return deferred.resolve(result, relatedObjs);
            });
          } else if (keyUpdates) {
            g = _this.__updateForeignKeyRelations(keyUpdates, obj[_this.primaryKey]);
            return g.then(function(relatedObjs) {
              console.log("Saved " + _this.name + " and updated Keys on Relations", saveObj, relatedObjs);
              return deferred.resolve(result, relatedObjs);
            });
          } else {
            console.log("Saved " + _this.name, saveObj);
            return deferred.resolve(result);
          }
        });
        p.fail(function(e) {
          return deferred.reject(e);
        });
        p.done();
      }
      return deferred.promise;
    };

    Model.prototype.query = function() {
      return new JohoDB.Queryset(this);
    };

    return Model;

  })();

  window.JohoDB.Queryset = (function() {
    function Queryset(model) {
      this.model = model;
      this.querySteps = [];
    }

    Queryset.prototype.all = function() {
      this.querySteps.push({
        action: 'all',
        args: {}
      });
      return this;
    };

    Queryset.prototype.get = function(args) {
      var k, newStep, v;

      newStep = {
        action: 'get',
        args: (function() {
          var _results;

          _results = [];
          for (k in args) {
            v = args[k];
            _results.push(new JohoDB.FieldLookup(k, v));
          }
          return _results;
        })()
      };
      if (newStep.args.length < 1) {
        console.error("No arguments provided for get", args);
        throw "No arguments provided for get";
      }
      this.querySteps.push(newStep);
      return this;
    };

    Queryset.prototype.filter = function(args) {
      var k, newStep, v;

      newStep = {
        action: "filter",
        args: (function() {
          var _results;

          _results = [];
          for (k in args) {
            v = args[k];
            _results.push(new JohoDB.FieldLookup(k, v));
          }
          return _results;
        })()
      };
      if (newStep.args.length < 1) {
        console.error("No arguments provided for filter", args);
        throw "No arguments provided for filter";
      }
      this.querySteps.push(newStep);
      return this;
    };

    Queryset.prototype.exclude = function(args) {
      var k, newStep, v;

      newStep = {
        action: "exclude",
        args: (function() {
          var _results;

          _results = [];
          for (k in args) {
            v = args[k];
            _results.push(new JohoDB.FieldLookup(k, v));
          }
          return _results;
        })()
      };
      if (newStep.args.length < 1) {
        console.error("No arguments provided for filter", args);
        throw "No arguments provided for filter";
      }
      this.querySteps.push(newStep);
      return this;
    };

    Queryset.prototype.hasStep = function(stepName) {
      var step, _i, _len, _ref6;

      _ref6 = this.querySteps;
      for (_i = 0, _len = _ref6.length; _i < _len; _i++) {
        step = _ref6[_i];
        if (step.action === stepName) {
          return true;
        }
      }
      return false;
    };

    Queryset.prototype.getStep = function(stepName) {
      var step, _i, _len, _ref6;

      _ref6 = this.querySteps;
      for (_i = 0, _len = _ref6.length; _i < _len; _i++) {
        step = _ref6[_i];
        if (step.action === stepName) {
          return step;
        }
      }
      return null;
    };

    Queryset.prototype.evaluate = function(opts) {
      var deferred, p,
        _this = this;

      if (!opts) {
        opts = {};
      }
      if (opts.depth === void 0) {
        opts.depth = 1;
      }
      deferred = Q.defer();
      p = this.model.store.evaluateQuery(this.model, this, opts);
      p = p.then(function(result) {
        var g, obj, stack, _i, _len;

        if (opts != null ? opts.depth : void 0) {
          if (Array.isArray(result)) {
            stack = [];
            for (_i = 0, _len = result.length; _i < _len; _i++) {
              obj = result[_i];
              stack.push(_this.__getRelatedObjects(obj, {
                depth: opts.depth - 1
              }));
            }
            g = Q.all(stack);
          } else {
            g = _this.__getRelatedObjects(result, {
              depth: opts.depth - 1
            });
          }
          return g.then(function(relatedObjs) {
            _this.__zipRelatedObjs(result, relatedObjs);
            return deferred.resolve(result);
          });
        } else {
          return deferred.resolve(result);
        }
      });
      p.fail(function(e) {
        return deferred.reject(e);
      });
      p.done();
      return deferred.promise;
    };

    Queryset.prototype.__getRelatedObjects = function(obj, opts) {
      var func, name, query, relation, stack, _ref6;

      stack = [];
      _ref6 = this.model.relations;
      for (name in _ref6) {
        relation = _ref6[name];
        if (relation.type === 'ForeignKey' && obj[name] !== void 0) {
          query = {};
          query[relation.relatedKey] = obj[name];
          stack.push(relation.model.query().get(query).evaluate(opts));
        } else if (relation.type === 'OneToMany') {
          query = {};
          query[relation.relatedName] = obj[this.model.primaryKey];
          stack.push(relation.model.query().filter(query).evaluate(opts));
        } else if (relation.type === 'ManyToMany') {
          stack.push(this.model.store.collectManyToManyRelations(relation, obj[this.model.primaryKey]));
        } else {
          func = function() {
            return [];
          };
          stack.push(func());
        }
      }
      return Q.all(stack);
    };

    Queryset.prototype.__zipRelatedObjs = function(result, relatedObjs) {
      var i, res, _i, _len, _results;

      if (Array.isArray(result)) {
        i = 0;
        _results = [];
        for (_i = 0, _len = result.length; _i < _len; _i++) {
          res = result[_i];
          this.__zipRelatedObj(res, relatedObjs[i]);
          _results.push(i += 1);
        }
        return _results;
      } else {
        return this.__zipRelatedObj(result, relatedObjs);
      }
    };

    Queryset.prototype.__zipRelatedObj = function(obj, relatedObjs) {
      var i, name, relation, _ref6, _results;

      i = 0;
      _ref6 = this.model.relations;
      _results = [];
      for (name in _ref6) {
        relation = _ref6[name];
        obj[name] = relatedObjs[i];
        _results.push(i += 1);
      }
      return _results;
    };

    return Queryset;

  })();

  window.JohoDB.FieldLookup = (function() {
    function FieldLookup(name, value) {
      var field, lookupType, _ref6;

      _ref6 = name.split('__'), field = _ref6[0], lookupType = _ref6[1];
      this.field = field;
      this.type = lookupType || 'exact';
      this.value = value;
    }

    return FieldLookup;

  })();

  'use strict';

  window.JohoDB.Relation = (function() {
    function Relation(fromModelName, fromField, skipCheck) {
      if (!skipCheck) {
        if (fromField.type === "ForeignKey") {
          return new JohoDB.ForeignKeyRelation(fromModelName, fromField, true);
        }
        if (fromField.type === "ManyToMany") {
          return new JohoDB.ManyToManyRelation(fromModelName, fromField, true);
        }
      }
      this.type = fromField.type;
      this.from = {
        modelName: fromModelName,
        fieldName: fromField.name
      };
      this.to = {
        modelName: fromField.relation,
        fieldName: fromField.relatedName
      };
      if (fromField.onDelete) {
        this.onDelete = fromField.onDelete;
      } else {
        this.onDelete = "cascade";
      }
    }

    Relation.prototype.buildRelationManagers = function() {
      throw "buildRelationManagers Not Implemented";
    };

    Relation.prototype.buildRelationTable = function() {
      throw "buildRelationTable Not Implemented";
    };

    Relation.prototype.getTableName = function() {
      throw "getTableName Not Implemented";
    };

    return Relation;

  })();

  window.JohoDB.ForeignKeyRelation = (function(_super) {
    __extends(ForeignKeyRelation, _super);

    function ForeignKeyRelation() {
      _ref6 = ForeignKeyRelation.__super__.constructor.apply(this, arguments);
      return _ref6;
    }

    ForeignKeyRelation.prototype.requiresRelationTable = false;

    ForeignKeyRelation.prototype.buildRelationManagers = function() {
      var fromManager, toManager;

      fromManager = new JohoDB.ForeignKeyRelationManager(this.from.fieldName, this.to.fieldName, null, this.from.modelName, this.to.modelName, this.to.primaryKey, this.from.primaryKey, null);
      toManager = new JohoDB.OneToManyRelationManager(this.to.fieldName, this.from.fieldName, null, this.to.modelName, this.from.modelName, this.from.primaryKey, this.to.primaryKey, this.onDelete);
      return [fromManager, toManager];
    };

    return ForeignKeyRelation;

  })(JohoDB.Relation);

  window.JohoDB.ManyToManyRelation = (function(_super) {
    __extends(ManyToManyRelation, _super);

    function ManyToManyRelation() {
      _ref7 = ManyToManyRelation.__super__.constructor.apply(this, arguments);
      return _ref7;
    }

    ManyToManyRelation.prototype.requiresRelationTable = true;

    ManyToManyRelation.prototype.buildRelationManagers = function() {
      var fromManager, toManager;

      fromManager = new JohoDB.ManyToManyRelationManager(this.from.fieldName, this.to.fieldName, this.getTableName(), this.from.modelName, this.to.modelName, this.to.primaryKey, this.from.primaryKey, null);
      toManager = new JohoDB.ManyToManyRelationManager(this.to.fieldName, this.from.fieldName, this.getTableName(), this.to.modelName, this.from.modelName, this.from.primaryKey, this.to.primaryKey, null);
      return [fromManager, toManager];
    };

    ManyToManyRelation.prototype.getTableName = function() {
      return "" + this.from.modelName + "_" + this.from.fieldName + "__" + this.to.modelName + "_" + this.to.fieldName;
    };

    ManyToManyRelation.prototype.buildRelationTable = function() {
      return new JohoDB.RelationTable(this.getTableName(), this.from, this.to);
    };

    return ManyToManyRelation;

  })(JohoDB.Relation);

  window.JohoDB.RelationTable = (function() {
    function RelationTable(name, from, to) {
      var fromName, toName;

      this.name = name;
      this.fields = {};
      this.fields["id"] = new JohoDB.Field("id", {
        type: "string",
        primaryKey: true,
        autoHash: true
      });
      this.primaryKey = "id";
      fromName = "" + from.modelName + "_" + from.primaryKey;
      this.fields[fromName] = new JohoDB.Field(fromName, {
        type: "int",
        required: true
      });
      toName = "" + to.modelName + "_" + to.primaryKey;
      this.fields[toName] = new JohoDB.Field(toName, {
        type: "int",
        required: true
      });
    }

    return RelationTable;

  })();

  window.JohoDB.ModelRelationManager = (function() {
    function ModelRelationManager(name, relatedName, relationTableName, ownModel, relatedModel, relatedKey, ownKey, onDelete) {
      this.name = name;
      this.relatedName = relatedName;
      this.relationTableName = relationTableName;
      this.ownModel = ownModel;
      this.relatedModel = relatedModel;
      this.relatedKey = relatedKey;
      this.ownKey = ownKey;
      this.onDelete = onDelete;
    }

    ModelRelationManager.prototype.validate = function(value) {
      return console.error("Validate not implemented.");
    };

    return ModelRelationManager;

  })();

  window.JohoDB.ForeignKeyRelationManager = (function(_super) {
    __extends(ForeignKeyRelationManager, _super);

    function ForeignKeyRelationManager() {
      _ref8 = ForeignKeyRelationManager.__super__.constructor.apply(this, arguments);
      return _ref8;
    }

    ForeignKeyRelationManager.prototype.type = 'ForeignKey';

    ForeignKeyRelationManager.prototype.validate = function(value) {
      if (typeof value === "object") {
        if (value[this.relatedKey] === void 0) {
          console.error("No primary key provided for relation " + this.name, value);
          throw "Validation Eror";
        }
      }
      if (Array.isArray(value)) {
        console.error("Foreign Key " + this.name + " doesn't support arrays.", value);
        throw "Validation Eror";
      }
    };

    ForeignKeyRelationManager.prototype.reduce = function(value) {
      if (typeof value === "object") {
        return value[this.relatedKey];
      } else {
        return value;
      }
    };

    ForeignKeyRelationManager.prototype.collectRelatedObjects = function(value, reversePk) {
      if (typeof value === "object") {
        return [value];
      } else {
        return [];
      }
    };

    return ForeignKeyRelationManager;

  })(JohoDB.ModelRelationManager);

  window.JohoDB.OneToManyRelationManager = (function(_super) {
    __extends(OneToManyRelationManager, _super);

    function OneToManyRelationManager() {
      _ref9 = OneToManyRelationManager.__super__.constructor.apply(this, arguments);
      return _ref9;
    }

    OneToManyRelationManager.prototype.type = 'OneToMany';

    OneToManyRelationManager.prototype.validate = function(value) {
      var rel, _i, _len, _results;

      if (Array.isArray(value)) {
        _results = [];
        for (_i = 0, _len = value.length; _i < _len; _i++) {
          rel = value[_i];
          if (typeof value === "object") {
            if (rel[this.relatedKey] === void 0) {
              console.error("No primary key provided for relation " + this.name, value);
              throw "Validation Eror";
            } else {
              _results.push(void 0);
            }
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      }
    };

    OneToManyRelationManager.prototype.collectRelatedObjects = function(value, reversePk) {
      var obj, _i, _len;

      if (Array.isArray(value)) {
        for (_i = 0, _len = value.length; _i < _len; _i++) {
          obj = value[_i];
          obj[this.relatedName] = reversePk;
        }
        return value;
      } else {
        return [];
      }
    };

    OneToManyRelationManager.prototype.collectKeys = function(value) {
      var keys, obj, _i, _len;

      keys = [];
      if (Array.isArray(value)) {
        for (_i = 0, _len = value.length; _i < _len; _i++) {
          obj = value[_i];
          keys.push(obj[this.model.primaryKey]);
        }
      }
      return keys;
    };

    return OneToManyRelationManager;

  })(JohoDB.ModelRelationManager);

  window.JohoDB.ManyToManyRelationManager = (function(_super) {
    __extends(ManyToManyRelationManager, _super);

    function ManyToManyRelationManager() {
      _ref10 = ManyToManyRelationManager.__super__.constructor.apply(this, arguments);
      return _ref10;
    }

    ManyToManyRelationManager.prototype.type = 'ManyToMany';

    ManyToManyRelationManager.prototype.validate = function(value) {
      var rel, _i, _len, _results;

      if (Array.isArray(value)) {
        _results = [];
        for (_i = 0, _len = value.length; _i < _len; _i++) {
          rel = value[_i];
          if (typeof value === "object") {
            if (rel[this.relatedKey] === void 0) {
              console.error("No primary key provided for relation " + this.name, value);
              throw "Validation Eror";
            } else {
              _results.push(void 0);
            }
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      }
    };

    ManyToManyRelationManager.prototype.collectRelatedObjects = function(value, reversePk) {
      if (Array.isArray(value)) {
        return value;
      } else {
        return [];
      }
    };

    ManyToManyRelationManager.prototype.collectKeys = function(value, reversePk) {
      var obj, relObj, rels, _i, _len;

      rels = [];
      if (Array.isArray(value)) {
        for (_i = 0, _len = value.length; _i < _len; _i++) {
          obj = value[_i];
          relObj = {};
          relObj[this.ownModel + '_' + this.ownKey] = reversePk;
          relObj[this.relatedModel + '_' + this.relatedKey] = obj[this.model.primaryKey];
          rels.push(relObj);
        }
      }
      return rels;
    };

    return ManyToManyRelationManager;

  })(JohoDB.ModelRelationManager);

  'use strict';

  window.JohoDB.BaseStorage = (function() {
    function BaseStorage(name, version) {
      this.name = name;
      this.version = version;
    }

    BaseStorage.prototype.initDB = function(jsSchema) {
      throw "initDB Not implemented";
    };

    BaseStorage.prototype.modelMethods = {
      save: function(model, obj, opts) {
        throw "save not implemented";
      }
    };

    return BaseStorage;

  })();

  if (window.webkitIndexedDB) {
    window.indexedDB = window.webkitIndexedDB;
    window.IDBKeyRange = window.webkitIDBKeyRange;
    window.IDBTransaction = window.webkitIDBTransaction;
  }

  READ_WRITE_FLAG = "readwrite";

  __deleteCursorPromise = function(cursor) {
    var deferred, dreq;

    deferred = Q.defer();
    dreq = cursor["delete"]();
    dreq.onsuccess = function(e) {
      return deferred.resolve(true);
    };
    return deferred.promise;
  };

  window.JohoDB.IndexedDbStorage = (function(_super) {
    __extends(IndexedDbStorage, _super);

    function IndexedDbStorage() {
      _ref11 = IndexedDbStorage.__super__.constructor.apply(this, arguments);
      return _ref11;
    }

    IndexedDbStorage.prototype.initDB = function(jsSchema, initOpts) {
      var clobber, dbDefer, req,
        _this = this;

      dbDefer = Q.defer();
      this.promise = dbDefer.promise;
      this.p = dbDefer.promise;
      clobber = Q.defer();
      if (initOpts.clobber) {
        req = window.indexedDB.deleteDatabase(this.name);
        req.onsuccess = function(e) {
          clobber.resolve(true);
          return console.warn("Clobbered Database " + _this.name);
        };
        req.onerror = function(e) {
          clobber.reject(e);
          throw e;
        };
      } else {
        clobber.resolve(true);
      }
      return clobber.promise.then(function(result) {
        return _this.__startDB(jsSchema, initOpts, dbDefer);
      });
    };

    IndexedDbStorage.prototype.__startDB = function(jsSchema, initOpts, dbDefer) {
      var req,
        _this = this;

      req = window.indexedDB.open(this.name, this.version);
      req.onsuccess = function(e) {
        var idb;

        idb = e.target.result;
        console.log('Finished loading', _this);
        return dbDefer.resolve(idb);
      };
      req.onupgradeneeded = function(e) {
        var idb;

        idb = e.target.result;
        console.log('Upgrading');
        return _this.__buildTables(idb, jsSchema, initOpts, e);
      };
      return req.onerror = function(e) {
        dbDefer.reject(e);
        throw e;
      };
    };

    IndexedDbStorage.prototype.__buildTables = function(idb, jsSchema, initOpts, e) {
      var model, modelName, relationName, relationTable, _ref12, _ref13, _results;

      _ref12 = jsSchema.models;
      for (modelName in _ref12) {
        model = _ref12[modelName];
        this.__buildTable(idb, model);
      }
      _ref13 = jsSchema.relationTables;
      _results = [];
      for (relationName in _ref13) {
        relationTable = _ref13[relationName];
        _results.push(this.__buildTable(idb, relationTable));
      }
      return _results;
    };

    IndexedDbStorage.prototype.__buildTable = function(idb, model) {
      var field, name, store, _ref12, _results;

      store = idb.createObjectStore(model.name, {
        keyPath: model.primaryKey,
        autoIncrement: true
      });
      _ref12 = model.fields;
      _results = [];
      for (name in _ref12) {
        field = _ref12[name];
        _results.push(field.makeIDBIndex(store));
      }
      return _results;
    };

    IndexedDbStorage.prototype.evaluateQuery = function(model, queryset, opts) {
      var deferred,
        _this = this;

      deferred = Q.defer();
      this.promise.then(function(idb) {
        var lookups;

        if (queryset.hasStep("get")) {
          lookups = queryset.getStep("get").args;
          return _this.__get(model, idb, lookups, opts).then(function(result) {
            return deferred.resolve(result);
          });
        } else if (queryset.hasStep("all")) {
          return _this.__getAll(model, idb, opts).then(function(result) {
            return deferred.resolve(result);
          });
        } else {
          return _this.__executeQuery(model, idb, queryset.querySteps, opts).then(function(result) {
            return deferred.resolve(result);
          });
        }
      });
      return deferred.promise;
    };

    IndexedDbStorage.prototype.__executeQuery = function(model, idb, steps, opts) {
      var deferred, leadLookup, leadStep, req, results;

      deferred = Q.defer();
      results = [];
      leadStep = steps[0];
      leadLookup = leadStep.args.shift();
      req = idb.transaction(model.name).objectStore(model.name).index(leadLookup.getIDBIndex()).openCursor(leadLookup.getIDBKeyRange());
      req.onsuccess = function(e) {
        var cursor;

        cursor = e.target.result;
        if (cursor) {
          if (matchSteps(steps, cursor.value)) {
            results.push(cursor.value);
            return cursor["continue"]();
          } else {
            return cursor["continue"]();
          }
        } else {
          return deferred.resolve(results);
        }
      };
      req.onerror = function(e) {
        return deferred.reject(e);
      };
      return deferred.promise;
    };

    IndexedDbStorage.prototype.__get = function(model, idb, lookups, opts) {
      var deferred, leadLookup, req;

      deferred = Q.defer();
      leadLookup = lookups.shift();
      req = idb.transaction(model.name).objectStore(model.name).index(leadLookup.getIDBIndex()).openCursor(leadLookup.getIDBKeyRange());
      req.onsuccess = function(e) {
        var cursor;

        cursor = e.target.result;
        if (cursor) {
          if (matchLookups(lookups, cursor.value)) {
            return deferred.resolve(cursor.value);
          } else {
            return cursor["continue"]();
          }
        } else {
          return deferred.resolve(null);
        }
      };
      req.onerror = function(e) {};
      return deferred.promise;
    };

    IndexedDbStorage.prototype.__getAll = function(model, idb, opts) {
      var deferred, req, results;

      deferred = Q.defer();
      results = [];
      req = idb.transaction(model.name).objectStore(model.name).openCursor();
      req.onsuccess = function(e) {
        var cursor;

        cursor = e.target.result;
        if (cursor) {
          results.push(cursor.value);
          return cursor["continue"]();
        } else {
          return deferred.resolve(results);
        }
      };
      req.onerror = function(e) {
        return deferred.reject(e);
      };
      return deferred.promise;
    };

    IndexedDbStorage.prototype.collectManyToManyRelations = function(relation, startPk) {
      var deferred, fetches;

      deferred = Q.defer();
      fetches = [];
      this.promise.then(function(idb) {
        var req;

        req = idb.transaction(relation.relationTableName).objectStore(relation.relationTableName).index(relation.ownModel + '_' + relation.ownKey).openCursor(IDBKeyRange.only(startPk));
        req.onsuccess = function(e) {
          var cursor, fetchStack, objKey, query, rel;

          cursor = e.target.result;
          if (cursor) {
            rel = cursor.value;
            objKey = rel["" + relation.relatedModel + "_" + relation.relatedKey];
            query = {};
            query[relation.model.primaryKey] = objKey;
            fetches.push(relation.model.query().get(query).evaluate({
              depth: 0
            }));
            return cursor["continue"]();
          } else {
            fetchStack = Q.all(fetches);
            return fetchStack.then(function(objs) {
              return deferred.resolve(objs);
            });
          }
        };
        return req.onerror = function(e) {
          return console.error(e);
        };
      });
      return deferred.promise;
    };

    IndexedDbStorage.prototype.clearManyToManyRelations = function(relationTable, objID, objField) {
      var deferred;

      deferred = Q.defer();
      this.promise.then(function(idb) {
        var deletes, req;

        deletes = [];
        req = idb.transaction(relationTable, READ_WRITE_FLAG).objectStore(relationTable).index(objField).openCursor(IDBKeyRange.only(objID));
        req.onsuccess = function(e) {
          var cursor, dp;

          cursor = e.target.result;
          if (cursor) {
            deletes.push(__deleteCursorPromise(cursor));
            return cursor["continue"]();
          } else {
            dp = Q.all(deletes);
            return dp.then(function() {
              return deferred.resolve(true);
            });
          }
        };
        return req.onerror = function(e) {
          console.error(e);
          return deferred.reject(e);
        };
      });
      return deferred.promise;
    };

    IndexedDbStorage.prototype.__makeManyToManyRelation = function(idb, relationTable, relation) {
      var deferred, req, string;

      deferred = Q.defer();
      string = JSON.stringify(relation);
      relation.id = CryptoJS.SHA1(string + new Date().getTime()).toString();
      req = idb.transaction(relationTable, READ_WRITE_FLAG).objectStore(relationTable).add(relation);
      req.onsuccess = function(e) {
        return deferred.resolve(relation);
      };
      return deferred.promise;
    };

    IndexedDbStorage.prototype.createManyToManyRelations = function(relationTable, relations) {
      var deferred,
        _this = this;

      deferred = Q.defer();
      this.promise.then(function(idb) {
        var relation, saves, sp, _i, _len;

        saves = [];
        for (_i = 0, _len = relations.length; _i < _len; _i++) {
          relation = relations[_i];
          saves.push(_this.__makeManyToManyRelation(idb, relationTable, relation));
        }
        sp = Q.all(saves);
        return sp.then(function(result) {
          return deferred.resolve(true);
        });
      });
      return deferred.promise;
    };

    IndexedDbStorage.prototype.updateForeignKey = function(model, objID, fieldName, reverseID) {
      var deferred;

      deferred = Q.defer();
      this.promise.then(function(idb) {
        var req;

        req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).openCursor(IDBKeyRange.only(objID));
        return req.onsuccess = function(e) {
          var cursor, obj, ureq;

          cursor = e.target.result;
          if (cursor) {
            obj = cursor.value;
            obj[fieldName] = reverseID;
            ureq = cursor.update(obj);
            return ureq.onsuccess = function(e) {
              return deferred.resolve(obj);
            };
          } else {
            console.error("Foreign Key Update Failed");
            return deferred.reject(false);
          }
        };
      });
      return deferred.promise;
    };

    IndexedDbStorage.prototype["delete"] = function(model, id, opts) {
      var deferred,
        _this = this;

      deferred = Q.defer();
      this.promise.then(function(idb) {
        var req;

        req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).openCursor(IDBKeyRange.only(id));
        req.onsuccess = function(e) {
          var cursor, ureq;

          cursor = e.target.result;
          if (cursor) {
            ureq = cursor["delete"]();
            ureq.onsuccess = function(e) {
              return deferred.resolve(true);
            };
            return ureq.onerror = function(e) {
              return deferred.reject(e);
            };
          } else {
            return deferred.resolve(true);
          }
        };
        return req.onerror = function(e) {
          return deferred.reject(e);
        };
      });
      return deferred.promise;
    };

    IndexedDbStorage.prototype.save = function(model, saveObj, opts) {
      var deferred;

      deferred = Q.defer();
      this.promise.then(function(idb) {
        var curreq, preSave, saveMethod;

        preSave = Q.defer();
        saveMethod = void 0;
        if (saveObj[model.primaryKey] === void 0) {
          preSave.resolve("add");
        } else {
          curreq = idb.transaction(model.name).objectStore(model.name).openCursor(IDBKeyRange.only(saveObj[model.primaryKey]));
          curreq.onsuccess = function(e) {
            var cursor;

            cursor = e.target.result;
            if (cursor) {
              return preSave.resolve("update");
            } else {
              return preSave.resolve("add");
            }
          };
          curreq.onerror = function(e) {
            throw e;
          };
        }
        return preSave.promise.then(function(method) {
          var req;

          if (method === "add") {
            req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).add(saveObj);
            req.onsuccess = function(e) {
              return deferred.resolve(saveObj);
            };
            req.onerror = function(e) {
              return deferred.reject(e);
            };
          }
          if (method === "update") {
            req = idb.transaction(model.name, READ_WRITE_FLAG).objectStore(model.name).openCursor(IDBKeyRange.only(saveObj[model.primaryKey]));
            return req.onsuccess = function(e) {
              var cursor, ureq;

              cursor = e.target.result;
              ureq = cursor.update(saveObj);
              return ureq.onsuccess = function(e) {
                return deferred.resolve(saveObj);
              };
            };
          }
        });
      });
      return deferred.promise;
    };

    return IndexedDbStorage;

  })(JohoDB.BaseStorage);

  JohoDB.Field.prototype.makeIDBIndex = function(store) {
    var opts;

    opts = {};
    if (this.primaryKey) {
      opts.unique = true;
    } else {
      opts.unique = false;
    }
    if (this.unique) {
      opts.unique = true;
    }
    return store.createIndex(this.name, this.name, opts);
  };

  JohoDB.ManyToManyField.prototype.makeIDBIndex = function(store) {
    return null;
  };

  matchSteps = function(steps, obj) {
    var step, wasFilteredIn, _i, _len;

    wasFilteredIn = false;
    for (_i = 0, _len = steps.length; _i < _len; _i++) {
      step = steps[_i];
      if (step.action === 'exclude') {
        if (matchLookups(step.args, obj)) {
          return false;
        }
      }
      if (step.action === 'filter') {
        if (matchLookups(step.args, obj)) {
          wasFilteredIn = true;
        }
      }
    }
    return wasFilteredIn;
  };

  matchLookups = function(lookups, obj) {
    var lookup, _i, _len;

    if (lookups.length === 0) {
      return true;
    }
    for (_i = 0, _len = lookups.length; _i < _len; _i++) {
      lookup = lookups[_i];
      if (lookup.testIDBValue(obj)) {
        return true;
      }
    }
    return false;
  };

  JohoDB.FieldLookup.prototype.testIDBValue = function(obj) {
    var value;

    value = obj[this.field];
    if (this.type === "exact") {
      if (value === this.value) {
        return true;
      }
    }
    if (this.type === "gt") {
      if (value > this.value) {
        return true;
      }
    }
    if (this.type === "gte") {
      if (value >= this.value) {
        return true;
      }
    }
    if (this.type === "lt") {
      if (value < this.value) {
        return true;
      }
    }
    if (this.type === "lte") {
      if (value <= this.value) {
        return true;
      }
    }
    return false;
  };

  JohoDB.FieldLookup.prototype.getIDBIndex = function() {
    return this.field;
  };

  JohoDB.FieldLookup.prototype.getIDBKeyRange = function() {
    if (this.type === "exact") {
      return IDBKeyRange.only(this.value);
    }
    if (this.type === "gt") {
      return IDBKeyRange.lowerBound(this.value, true);
    }
    if (this.type === "gte") {
      return IDBKeyRange.lowerBound(this.value);
    }
    if (this.type === "lt") {
      return IDBKeyRange.upperBound(this.value, true);
    }
    if (this.type === "lte") {
      return IDBKeyRange.upperBound(this.value);
    }
    console.error("No valid lookup found for " + this.type);
    throw "No valid lookup found for " + this.type;
  };

  stSuc = function(log) {
    return console.log(log);
  };

  stErr = function(error) {
    console.error(error);
    return true;
  };

  window.JohoDB.WebSqlStorage = (function(_super) {
    __extends(WebSqlStorage, _super);

    function WebSqlStorage() {
      _ref12 = WebSqlStorage.__super__.constructor.apply(this, arguments);
      return _ref12;
    }

    WebSqlStorage.prototype.initDB = function(jsSchema, initOpts) {
      var clobber, db, dbDefer,
        _this = this;

      dbDefer = Q.defer();
      this.promise = dbDefer.promise;
      this.p = dbDefer.promise;
      db = openDatabase(this.name, this.version, 'JohoDB Database', 2 * 1024 * 1024);
      clobber = Q.defer();
      if (initOpts.clobber) {
        console.warn('Clobbering Database');
        db.transaction(function(tx) {
          return _this.__dropAllTables(tx, jsSchema);
        }, function(tx, error) {
          console.error("Failed Clobbering Database");
          return clobber.reject(error);
        }, function(tx, success) {
          return clobber.resolve(true);
        });
      } else {
        clobber.resolve(true);
      }
      return clobber.promise.then(function() {
        console.log('Building Tables');
        return db.transaction(function(tx) {
          return _this.__buildTables(tx, jsSchema, initOpts);
        }, function(tx, error) {
          return console.error("Failed building Database", tx, error);
        }, function(tx, success) {
          dbDefer.resolve(db);
          return console.log("Finished Loading", _this);
        });
      });
    };

    WebSqlStorage.prototype.__dropAllTables = function(tx, jsSchema) {
      var model, modelName, relationName, relationTable, _ref13, _ref14, _results;

      _ref13 = jsSchema.models;
      for (modelName in _ref13) {
        model = _ref13[modelName];
        tx.executeSql("DROP TABLE IF EXISTS " + model.name + ";");
      }
      _ref14 = jsSchema.relationTables;
      _results = [];
      for (relationName in _ref14) {
        relationTable = _ref14[relationName];
        _results.push(tx.executeSql("DROP TABLE IF EXISTS " + relationTable.name + ";"));
      }
      return _results;
    };

    WebSqlStorage.prototype.__buildTables = function(tx, jsSchema, initOpts) {
      var model, modelName, relationName, relationTable, _ref13, _ref14, _results;

      _ref13 = jsSchema.models;
      for (modelName in _ref13) {
        model = _ref13[modelName];
        this.__buildTable(tx, model);
      }
      _ref14 = jsSchema.relationTables;
      _results = [];
      for (relationName in _ref14) {
        relationTable = _ref14[relationName];
        _results.push(this.__buildTable(tx, relationTable));
      }
      return _results;
    };

    WebSqlStorage.prototype.__buildTable = function(tx, table) {
      var field, fieldStatements, name, sqlStatement, statement, _ref13;

      fieldStatements = [];
      _ref13 = table.fields;
      for (name in _ref13) {
        field = _ref13[name];
        statement = field.makeSQLField();
        if (statement) {
          fieldStatements.push(statement);
        }
      }
      sqlStatement = "CREATE TABLE IF NOT EXISTS " + table.name + " (";
      sqlStatement += fieldStatements.join(',');
      sqlStatement += ");";
      return tx.executeSql(sqlStatement);
    };

    WebSqlStorage.prototype.evaluateQuery = function(model, queryset, opts) {
      var deferred,
        _this = this;

      deferred = Q.defer();
      this.promise.then(function(db) {
        var lookups;

        if (queryset.hasStep("get")) {
          lookups = queryset.getStep("get").args;
          return _this.__get(model, db, lookups, opts).then(function(result) {
            return deferred.resolve(result);
          });
        } else if (queryset.hasStep("all")) {
          return _this.__getAll(model, db, opts).then(function(result) {
            return deferred.resolve(result);
          });
        } else {
          return _this.__executeQuery(model, db, queryset.querySteps, opts).then(function(result) {
            return deferred.resolve(result);
          });
        }
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.__toJsObject = function(obj) {
      var jsObj, key, val;

      jsObj = {};
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        val = obj[key];
        jsObj[key] = val;
      }
      return jsObj;
    };

    WebSqlStorage.prototype.__executeQuery = function(model, db, steps, opts) {
      var deferred, results,
        _this = this;

      deferred = Q.defer();
      results = [];
      this.promise.then(function(db) {
        return db.readTransaction(function(tx) {
          var sqlStatement;

          sqlStatement = "SELECT * FROM " + model.name + " WHERE ";
          sqlStatement += stepsToSQL(model, steps);
          sqlStatement += ";";
          return tx.executeSql(sqlStatement, null, function(tx, res) {
            var index, _i, _ref13, _results;

            _results = [];
            for (index = _i = 0, _ref13 = res.rows.length; 0 <= _ref13 ? _i < _ref13 : _i > _ref13; index = 0 <= _ref13 ? ++_i : --_i) {
              _results.push(results.push(_this.__toJsObject(res.rows.item(index))));
            }
            return _results;
          }, function(tx, error) {
            return console.error("Failed read", tx, error);
          });
        }, function(tx, error) {
          return console.error("Failed read", tx, error);
        }, function(tx, success) {
          return deferred.resolve(results);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.__get = function(model, db, lookups, opts) {
      var deferred, result,
        _this = this;

      deferred = Q.defer();
      result = null;
      this.promise.then(function(db) {
        return db.readTransaction(function(tx) {
          var sqlStatement;

          sqlStatement = "SELECT * FROM " + model.name + " WHERE ";
          sqlStatement += lookupsToSQL(model, lookups);
          sqlStatement += ";";
          return tx.executeSql(sqlStatement, null, function(tx, res) {
            if (res.rows.length > 0) {
              return result = _this.__toJsObject(res.rows.item(0));
            }
          }, function(tx, res) {
            return console.error("Failed read", tx, error);
          });
        }, function(tx, error) {
          return console.error("Failed read", tx, error);
        }, function(tx, success) {
          return deferred.resolve(result);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.__getAll = function(model, db, opts) {
      var deferred, results,
        _this = this;

      deferred = Q.defer();
      results = [];
      this.promise.then(function(db) {
        return db.readTransaction(function(tx) {
          return tx.executeSql("SELECT * FROM " + model.name + ";", null, function(tx, res) {
            var index, _i, _ref13, _results;

            _results = [];
            for (index = _i = 0, _ref13 = res.rows.length; 0 <= _ref13 ? _i < _ref13 : _i > _ref13; index = 0 <= _ref13 ? ++_i : --_i) {
              _results.push(results.push(_this.__toJsObject(res.rows.item(index))));
            }
            return _results;
          }, function(tx, result) {
            return console.error("Failed read", tx, error);
          });
        }, function(tx, error) {
          return console.error("Failed read", tx, error);
        }, function(tx, success) {
          return deferred.resolve(results);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.collectManyToManyRelations = function(r, startPk) {
      var deferred, results,
        _this = this;

      deferred = Q.defer();
      results = [];
      this.promise.then(function(db) {
        return db.readTransaction(function(tx) {
          var sql;

          sql = "SELECT * FROM " + r.relatedModel + " WHERE ";
          sql += "" + r.relatedKey + " IN (";
          sql += "SELECT " + r.relatedModel + "." + r.relatedKey + " FROM ";
          sql += "" + r.relatedModel + ", " + r.relationTableName + " WHERE ";
          sql += "" + r.relatedModel + "." + r.relatedKey + " = ";
          sql += "" + r.relationTableName + "." + r.relatedModel + "_" + r.relatedKey + " AND ";
          sql += "" + r.relationTableName + "." + r.ownModel + "_" + r.ownKey + " ";
          sql += "= '" + startPk + "');";
          return tx.executeSql(sql, null, function(tx, res) {
            var index, _i, _ref13, _results;

            _results = [];
            for (index = _i = 0, _ref13 = res.rows.length; 0 <= _ref13 ? _i < _ref13 : _i > _ref13; index = 0 <= _ref13 ? ++_i : --_i) {
              _results.push(results.push(_this.__toJsObject(res.rows.item(index))));
            }
            return _results;
          }, function(tx, error) {
            return console.error("Failed read", tx, error);
          });
        }, function(tx, error) {
          return console.error("Failed read", tx, error);
        }, function(tx, success) {
          return deferred.resolve(results);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.clearManyToManyRelations = function(relationTable, objID, objField) {
      var deferred;

      deferred = Q.defer();
      this.promise.then(function(db) {
        var _this = this;

        return db.transaction(function(tx) {
          var sqlStatement;

          sqlStatement = "DELETE FROM " + relationTable + " WHERE " + objField + " IN (";
          sqlStatement += "SELECT " + relationTable + "." + objField + " ";
          sqlStatement += "FROM " + relationTable + " WHERE ";
          sqlStatement += "" + relationTable + "." + objField + " = ?);";
          return tx.executeSql(sqlStatement, [objID]);
        }, function(tx, error) {
          return console.error("Failed clearing ManyToManyRelations", tx, error);
        }, function(tx, success) {
          return deferred.resolve(objID);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.__makeManyToManyRelation = function(tx, relationTable, relation) {
      var fieldList, key, shroud, sqlStatement, string, val, valueList;

      string = JSON.stringify(relation);
      relation.id = CryptoJS.SHA1(string + new Date().getTime()).toString();
      fieldList = [];
      valueList = [];
      shroud = [];
      for (key in relation) {
        val = relation[key];
        fieldList.push(key);
        valueList.push(val);
        shroud.push("?");
      }
      sqlStatement = "INSERT INTO " + relationTable + " (";
      sqlStatement += fieldList.join(',');
      sqlStatement += ") VALUES (";
      sqlStatement += shroud.join(',');
      sqlStatement += ");";
      return tx.executeSql(sqlStatement, valueList);
    };

    WebSqlStorage.prototype.createManyToManyRelations = function(relationTable, relations) {
      var deferred,
        _this = this;

      deferred = Q.defer();
      this.promise.then(function(db) {
        return db.transaction(function(tx) {
          var relation, _i, _len, _results;

          _results = [];
          for (_i = 0, _len = relations.length; _i < _len; _i++) {
            relation = relations[_i];
            _results.push(_this.__makeManyToManyRelation(tx, relationTable, relation));
          }
          return _results;
        }, function(tx, error) {
          return console.error("Failed m2m update", tx, error);
        }, function(tx, success) {
          return deferred.resolve(true);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.updateForeignKey = function(model, objID, fieldName, reverseID) {
      var deferred;

      deferred = Q.defer();
      this.promise.then(function(db) {
        var _this = this;

        return db.transaction(function(tx) {
          var sqlStatement;

          sqlStatement = "UPDATE " + model.name + " SET " + fieldName + " = ? ";
          sqlStatement += "WHERE " + model.primaryKey + " = ?;";
          return tx.executeSql(sqlStatement, [reverseID, objID]);
        }, function(tx, error) {
          return console.error("Failed key update", tx, error);
        }, function(tx, success) {
          return deferred.resolve(objID);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype["delete"] = function(model, id, opts) {
      var deferred,
        _this = this;

      deferred = Q.defer();
      this.promise.then(function(db) {
        return db.transaction(function(tx) {
          var sql;

          sql = "DELETE FROM " + model.name + " WHERE " + model.primaryKey + " = ?";
          return tx.executeSql(sql, [id]);
        }, function(tx, error) {
          return console.error("Failed delete.", tx, error);
        }, function(tx, success) {
          return deferred.resolve(true);
        });
      });
      return deferred.promise;
    };

    WebSqlStorage.prototype.save = function(model, saveObj, opts) {
      var deferred, key, val;

      deferred = Q.defer();
      for (key in saveObj) {
        val = saveObj[key];
        if (val === void 0) {
          saveObj[key] = "";
        }
      }
      this.promise.then(function(db) {
        var _this = this;

        return db.transaction(function(tx) {
          var fieldList, shroud, sqlStatement, valueList;

          fieldList = [];
          valueList = [];
          shroud = [];
          for (key in saveObj) {
            val = saveObj[key];
            fieldList.push(key);
            valueList.push(val);
            shroud.push("?");
          }
          sqlStatement = "INSERT OR REPLACE INTO " + model.name + " (";
          sqlStatement += fieldList.join(',');
          sqlStatement += ") VALUES (";
          sqlStatement += shroud.join(",");
          sqlStatement += ")";
          return tx.executeSql(sqlStatement, valueList);
        }, function(tx, error) {
          return console.error("Failed save", tx, error);
        }, function(tx, success) {
          return deferred.resolve(saveObj);
        });
      });
      return deferred.promise;
    };

    return WebSqlStorage;

  })(JohoDB.BaseStorage);

  JohoDB.Field.prototype.addSQLConstraints = function() {
    if (this.primaryKey) {
      return " PRIMARY KEY";
    } else if (this.unique) {
      return " UNIQUE";
    }
    return "";
  };

  JohoDB.Field.prototype.makeSQLField = function() {
    return '';
  };

  JohoDB.IntField.prototype.makeSQLField = function() {
    return "" + this.name + " INTEGER" + (this.addSQLConstraints());
  };

  JohoDB.TextField.prototype.makeSQLField = function() {
    return "" + this.name + " TEXT" + (this.addSQLConstraints());
  };

  JohoDB.DateTimeField.prototype.makeSQLField = function() {
    return "" + this.name + " DATETIME" + (this.addSQLConstraints());
  };

  JohoDB.ForeignKeyField.prototype.makeSQLField = function() {
    return "" + this.name + " TEXT" + (this.addSQLConstraints());
  };

  JohoDB.ManyToManyField.prototype.makeSQLField = function() {
    return '';
  };

  stepsToSQL = function(model, steps) {
    var groupClauses, step, _i, _len;

    groupClauses = [];
    for (_i = 0, _len = steps.length; _i < _len; _i++) {
      step = steps[_i];
      if (step.action === 'exclude') {
        groupClauses.push(excludesToSQL(model, step.args));
      }
      if (step.action === 'filter') {
        groupClauses.push(lookupsToSQL(model, step.args));
      }
    }
    return groupClauses.join(" AND ");
  };

  lookupsToSQL = function(model, lookups) {
    var clauses, lookup, _i, _len;

    clauses = [];
    for (_i = 0, _len = lookups.length; _i < _len; _i++) {
      lookup = lookups[_i];
      clauses.push("" + model.name + "." + (lookup.getSQLWhereClause()));
    }
    return clauses.join(" AND ");
  };

  excludesToSQL = function(model, lookups) {
    var clauses, lookup, _i, _len;

    clauses = [];
    for (_i = 0, _len = lookups.length; _i < _len; _i++) {
      lookup = lookups[_i];
      clauses.push("NOT " + model.name + "." + (lookup.getSQLWhereClause()));
    }
    return clauses.join(" AND ");
  };

  JohoDB.FieldLookup.prototype.getSQLWhereClause = function() {
    var operator;

    operator = (function() {
      switch (this.type) {
        case "exact":
          return "=";
        case "gt":
          return ">";
        case "gte":
          return ">=";
        case "lt":
          return "<";
        case "lte":
          return "=<";
        default:
          return console.error("No valid lookup found for " + this.type);
      }
    }).call(this);
    return "" + this.field + " " + operator + " \"" + this.value + "\"";
  };

}).call(this);
